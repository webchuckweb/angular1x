(function() {
  "use strict";

  angular.module('huntApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('app.home.hunt', {
        url: 'hunt/:huntId',
        views: {
          'content@': {
            templateUrl: 'app/hunt/hunt-view.tpl.html',
            controller: ['$stateParams', '$scope', '$state', 'dataService', huntViewCtrl],
            controllerAs: 'model'
          }
        }
      });
  });

  function huntViewCtrl($stateParams, $scope, $state, dataService) {
    var model = this;

    model.hunt = dataService.getHunt($stateParams.huntId);
  }

}());
