var app = angular.module('huntApp');

app.service('authService', ['$http', '$q', 'authToken', authServiceProvider]);
app.factory("authToken", ['$window', authTokenFactory]);
app.factory("authInterceptor", authInterceptor);

function authServiceProvider($http, $q, authToken) {

  var URLs = {
      login: '/login'
    },
    storage = localStorage,
    userObj = JSON.parse(storage.getItem('userObj'));

  function login(user, pass) {
    return $http.post(URLs.login, {
        username: user,
        password: pass
      })
      .then(function success(response) {
        console.log('login response', response);
        authToken.setToken(response.data.token);
        storage.setItem('userObj', JSON.stringify(response.data.user));
        return response;
      },
      function error(response) {
        console.log('login error', response);
      });
  }

  function logout() {
    authToken.setToken();
    storage.removeItem('userObj');
  }

  function isLoggedIn() {
    return (userObj) ? true : false;
  }

  function getUser() {
    return userObj;
  }

  return {
    login: login,
    logout: logout,
    isLoggedIn: isLoggedIn,
    getUser: getUser
  };
}

function authTokenFactory($window) {
  'use strict';

  var store = $window.localStorage;
  var key = 'auth-token';

  return {
    getToken: getToken,
    setToken: setToken
  };

  function getToken() {
    return store.getItem(key);
  }

  function setToken(token) {
    if (token) {
      store.setItem(key, token);
    } else {
      store.removeItem(key);
    }
  }
}

function authInterceptor(authToken) {
  'use strict';

  return {
    request: addToken
  };

  function addToken(config) {
    var token = authToken.getToken();
    if (token) {
      config.headers = config.headers || {};
      config.headers.Authorization = 'Bearer ' + token;
    }
    return config;
  }
}
