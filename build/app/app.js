(function() {
  "use strict";

  angular.module('huntApp', [
    'ngAnimate',
    'ui.router',
    'home',
    'conquests'
  ])
  .config(function ($stateProvider, $urlRouterProvider, $httpProvider) {

    $stateProvider
      .state('app', {
        url: '',
        abstract: true
      });

    $urlRouterProvider.otherwise('/');

    $httpProvider.interceptors.push('authInterceptor');
  });

}());
