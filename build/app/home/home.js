(function() {
  "use strict";

  var module = angular.module('home',[]);

  module.config( function($stateProvider) {
    $stateProvider
      .state('app.home', {
        url: '/',
        views: {
          'sidebar@': {
            templateUrl: 'app/common/sidebar.tpl.html',
            controller: 'sidebarCtrl as model'
          },
          'header@': {
            templateUrl: 'app/common/header.tpl.html'
          },
          'footer@': {
            templateUrl: 'app/common/footer.tpl.html'
          },
          'content@': {
            controller: 'conquestsCtrl as model',
            templateUrl: 'app/conquests/conquests.tpl.html'
          }
        },
        resolve: {
          user: function (authService, $state) {
            var user = authService.getUser();
            if(!user) {
              $state.go('app.login');
            }

            return user;
          }
        }
      })
      .state('app.login', {
        url: '/login',
        views: {
          'content@': {
            templateUrl: 'app/home/auth.tpl.html',
            controller: 'authCtrl',
            controllerAs: 'auth',
          }
        }
      });
  });

  module.controller('authCtrl', function($scope, $state, authService) {
    var model = this;

    model.username = 'test';
    model.password = 'test';

    model.login = function(username, password) {
      authService.login(username, password)
        .then( function(res) {
          $state.go('app.home');
        });
    };

    model.register = function(userObj) {
      console.log('register', userObj);
    };
  });

  module.controller('sidebarCtrl', function($scope, $state, authService) {
    var model = this;

    if(authService.getUser()) {
      $scope.userObj = {
        isLoggedIn: authService.isLoggedIn(),
        user: authService.getUser()
      };
    }

    model.logout = function() {
      authService.logout();
      $scope.userObj = null;
      $state.go('app.login');
    };
  });

} ());
