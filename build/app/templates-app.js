(function(module) {
try {
  module = angular.module('huntApp');
} catch (e) {
  module = angular.module('huntApp', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('app/common/footer.tpl.html',
    '<div class="footer">Copyright &copy; 2016, WebChuck Development, LLC</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('huntApp');
} catch (e) {
  module = angular.module('huntApp', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('app/common/header.tpl.html',
    '<h1>HUNT</h1>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('huntApp');
} catch (e) {
  module = angular.module('huntApp', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('app/common/sidebar.tpl.html',
    '<div class="app-menu">\n' +
    '  <ul>\n' +
    '    <li class="menu-item">\n' +
    '      <a href="/">Home</a>\n' +
    '    </li>\n' +
    '    <li class="menu-item">\n' +
    '      <a ui-sref="hunts">My Hunts</a>\n' +
    '    </li>\n' +
    '    <li class="menu-item">\n' +
    '      <a ui-sref="/search">Find a Hunt</a>\n' +
    '    </li>\n' +
    '    <li class="menu-item" ng-show="userObj.isLoggedIn">\n' +
    '      <a ng-click="model.logout()">Logout</a>\n' +
    '    </li>\n' +
    '  </ul>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('huntApp');
} catch (e) {
  module = angular.module('huntApp', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('app/conquests/conquests.tpl.html',
    '<div class="panel panel-default">\n' +
    '  <div class="panel-heading">Recent Conquests</div>\n' +
    '  <div class="panel-body">\n' +
    '    <div class="col-sm-4 col-md-3" ng-repeat="hunt in model.hunts">\n' +
    '      <a class="thumbnail" ui-sref="app.home.hunt({huntId: hunt.huntId})">\n' +
    '        <img ng-src="{{hunt.thumbnail}}" alt="{{hunt.title}}" />\n' +
    '        <div class="caption">\n' +
    '          {{hunt.title}}\n' +
    '          <span class="badge">{{hunt.clues.length}} clues</span>\n' +
    '        </div>\n' +
    '      </a>\n' +
    '    </div>\n' +
    '  </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('huntApp');
} catch (e) {
  module = angular.module('huntApp', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('app/home/auth.tpl.html',
    '<div class="col-sm-6 col-md-6 col-md-offset-3">\n' +
    '  <div class="panel panel-default">\n' +
    '    <div class="panel-body">\n' +
    '      <form id="login" ng-submit="auth.login(auth.username, auth.password)">\n' +
    '        <label>Username: <input type="text" ng-model="auth.username"/></label><br/>\n' +
    '        <label>Password: <input type="password" ng-model="auth.password"/></label><br/>\n' +
    '        <button type="submit">Login</button>\n' +
    '      </form>\n' +
    '    </div>\n' +
    '  </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('huntApp');
} catch (e) {
  module = angular.module('huntApp', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('app/home/home.tpl.html',
    '<div class="container-fluid">\n' +
    '  <div class="row">\n' +
    '    <div class="col-sm-3 col-md-2 sidebar">\n' +
    '      <div class="app-menu">\n' +
    '        <ul>\n' +
    '          <li class="menu-item">\n' +
    '            <a href="/">Home</a>\n' +
    '          </li>\n' +
    '          <li class="menu-item">\n' +
    '            <a ui-sref="hunts">My Hunts</a>\n' +
    '          </li>\n' +
    '          <li class="menu-item">\n' +
    '            <a ui-sref="/search">Find a Hunt</a>\n' +
    '          </li>\n' +
    '          <li class="menu-item" ng-show="userObj.isLoggedIn">\n' +
    '            <a ng-click="model.logout()">Logout</a>\n' +
    '          </li>\n' +
    '        </ul>\n' +
    '      </div>\n' +
    '    </div>\n' +
    '    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">\n' +
    '      <div class="container-fluid">\n' +
    '        <div class="row">\n' +
    '          <div class="home-view" ui-view="auth-view"></div>\n' +
    '        </div>\n' +
    '      </div>\n' +
    '    </div>\n' +
    '  </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('huntApp');
} catch (e) {
  module = angular.module('huntApp', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('app/hunt/hunt-view.tpl.html',
    '<h1>{{model.hunt.title}}</h1>\n' +
    '\n' +
    '<div class="panel panel-default">\n' +
    '  <div class="panel-body">\n' +
    '    <div class="col-sm-4 col-md-3 clues" ng-repeat="clue in model.hunt.clues">\n' +
    '      <a ui-sref="app.home.hunt.clue({huntId:model.hunt.huntId, clueId:clue.clueId})" class="thumbnail">\n' +
    '        <img ng-src="{{clue.thumbnail}}" alt="{{clue.title}}" />\n' +
    '        <h4>{{clue.title}}</h4>\n' +
    '        <p>{{clue.text}}</p>\n' +
    '      </a>\n' +
    '    </div>\n' +
    '  </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('huntApp');
} catch (e) {
  module = angular.module('huntApp', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('app/hunt/clue/clue-view.tpl.html',
    '<h1>{{model.clue.title}}</h1>\n' +
    '\n' +
    '<div class="panel panel-default">\n' +
    '  <div class="panel-body">\n' +
    '    <img ng-src="{{model.clue.imageFullUrl}}" alt="{{model.clue.title}}" />\n' +
    '    <h4>{{model.clue.title}}</h4>\n' +
    '    <p>{{model.clue.text}}</p>\n' +
    '  </div>\n' +
    '</div>\n' +
    '');
}]);
})();
