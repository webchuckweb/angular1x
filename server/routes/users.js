var express = require('express');
var router = express.Router();
var fs = require('fs');
var datafile = 'server/data/users.json';

/* GET users listing. */
router.get('/', function(req, res) {
  var data = fs.readFileSync(datafile, 'utf8');
  res.send(JSON.parse(data));
});

/* GET individual users */
router.route('/:id')

    .get(function(req, res) {

        //console.log('Retrieving book id: ' + req.params.id);

        var data = fs.readFileSync(datafile, 'utf8');
        var users = JSON.parse(data);

        var matchingUsers = users.filter(function(item) {
            return item.id == req.params.id;
        });

        if(matchingUsers.length === 0) {
            res.sendStatus(404);
        } else {
            res.send(matchingUsers[0]);
        }
    });

module.exports = router;
