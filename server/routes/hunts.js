var express = require('express');
var fs = require('fs');
var datafile = 'server/data/creations.json';
var router = express.Router();

/* GET all books and POST new books */
router.route('/')
    .get(function(req, res) {
        var data = fs.readFileSync(datafile, 'utf8');
        res.send(JSON.parse(data));
    })

    .post(function(req, res) {

        var data = getHuntData();
        var nextID = getNextAvailableID(data);

        var newHunt = {
            hunt_id: nextID,
            title: req.body.title,
            author: req.body.author,
            year_published: req.body.year_published
        };

        data.push(newHunt);

        saveHuntData(data);

//        res.set('Content-Type', 'application/json');
        res.status(201).send(newHunt);
    });


/* GET, PUT and DELETE individual books */
router.route('/:id')

    .get(function(req, res) {

        //console.log('Retrieving book id: ' + req.params.id);

        var data = getHuntData();

        var matchingHunts = data.filter(function(item) {
            return item.hunt_id == req.params.id;
        });

        if(matchingHunts.length === 0) {
            res.sendStatus(404);
        } else {
            res.send(matchingHunts[0]);
        }
    })

    .delete(function(req, res) {

        var data = getHuntData();

        var pos = data.map(function(e) {
            return e.hunt_id;
        }).indexOf(parseInt(req.params.id, 10));

        if (pos > -1) {
            data.splice(pos, 1);
        } else {
            res.sendStatus(404);
        }

        saveHuntData(data);
        res.sendStatus(204);

    });

function getNextAvailableID(allHunts) {

    var maxID = 0;

    allHunts.forEach(function(element, index, array) {

        if(element.hunt_id > maxID) {
            maxID = element.hunt_id;
        }

    });

    return ++maxID;

}

function getHuntData() {
    var data = fs.readFileSync(datafile, 'utf8');
    return JSON.parse(data);
}

function saveHuntData(data) {
    fs.writeFile(datafile, JSON.stringify(data, null, 4), function (err) {
        if (err) {
            console.log(err);
        }
    });
}

module.exports = router;
