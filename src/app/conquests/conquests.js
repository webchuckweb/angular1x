(function() {
  "use strict";

  var module = angular.module("conquests",['ui.router']);

  /*
  module.config(function ($stateProvider) {
    $stateProvider.
      state('app.conquests', {
        url: 'conquests',
        views: {
          'content@': {
            controller: ['dataService', conquestsCtrl],
            controllerAs: 'model',
            template: 'HELLO',
            //templateUrl: 'app/conquests/conquests.tpl.html'
          }
        }
      });
  });
  */

  module.controller('conquestsCtrl', function(dataService, $state, $scope, user) {
    var model = this;

    if(!user) {
      $state.go('login');
    }
    else {
      $scope.userObj = {
        isLoggedIn: true,
        user: user
      }
    }

    console.log('here in conquests', user);
    dataService.getConquests().then( function(res) {
      model.hunts = res;
    });

  });

}());
