angular.module('huntApp')
  .service('dataService', ['$http', '$q', dataServiceProvider]);

function dataServiceProvider($http, $q) {
  var URLs = {
      getHunts: '/api/hunts'
    },
    hunts,
    user;

  function getUserHunts() {
    return $http.get(URLs.getHunts).then( function(res) {
      console.log('res',res);
      hunts = res.data;
      return hunts;
    });
  }

  function getOwnedHunts() {

  }

  function getHunt(huntId) {
    if(!hunts || !hunts.length) {
      return getUserHunts().then(function(res) {
        hunts = res;
        return hunts;
      });
    }
    var index = _.findIndex(hunts, function(o) { return o.huntId == huntId; });
    if(index >= 0) {
      return hunts[index];
    }
  }

  function setHunt() {
    // if new, add to 'owned hunts' list
    addOwnedHunt();
  }

  function removeHunt() {
    // if removed, remove from 'owned hunts' list
  }

  function addUserHunt() {

  }

  function addOwnedHunt() {

  }

  function removeUserHunt() {

  }

  function removeOwnedHunt() {

  }

  function getUser() {

  }

  function setUser() {

  }

  return {
    getConquests: getUserHunts,
    getCreations: getOwnedHunts,
    getHunt: getHunt,
    getUser: getUser,
    addUserHunt: addUserHunt,
    removeUserHunt: removeUserHunt,
    saveHunt: setHunt,
    removeHunt: removeHunt,
    saveUser: setUser
  };
}
