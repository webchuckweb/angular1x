(function() {
  "use strict";

  angular.module('huntApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('app.home.hunt.clue', {
        url: 'hunt/:huntId/clue/:clueId',
        views: {
          'content@': {
            templateUrl: 'app/hunt/clue/clue-view.tpl.html',
            controller: ['$stateParams', 'dataService', clueViewCtrl],
            controllerAs: 'model'
          }
        }
      });
  });

  function clueViewCtrl($stateParams, dataService) {
    var model = this,
      clueIndex;
    console.log('state params', $stateParams);

    model.hunt = dataService.getHunt($stateParams.huntId);
    clueIndex = _.findIndex(model.hunt.clues,
      function(o) {
        console.log(o);
        return o.clueId == $stateParams.clueId;
      });

    if(clueIndex >= 0 && model.hunt.clues[clueIndex]) {
      model.clue = model.hunt.clues[clueIndex];
    }
    console.log('hunt in clueView', model.hunt);
    console.log('clue', model.clue);
  }

}());
